import { render, fireEvent } from "@testing-library/react";
import { LogOutButton } from "./logOutButton";
import { BrowserRouter } from "react-router-dom";
import { AuthContextProvider } from "../../context/authContext";

describe('LogOutButton', () => {
    test("calls the onClick function when clicked", () => {
        const handleLogOut = jest.fn(); //function mock
        const { getByRole } = render(<div onClick={handleLogOut}>
            <BrowserRouter>
            <AuthContextProvider>
                <LogOutButton></LogOutButton>
                </AuthContextProvider>
            </BrowserRouter>
        </div>)
        const button = getByRole('button');
        fireEvent.click(button);
        expect(handleLogOut).toHaveBeenCalledTimes(1);
});
});