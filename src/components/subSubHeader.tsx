type SubSubHeaderProps = {
    text: string;
};

function SubSubHeader({ text }: SubSubHeaderProps) {
    return <h3 className=" text-xl font-medium dark:text-slate-200">{text}</h3>;
}

export default SubSubHeader;
