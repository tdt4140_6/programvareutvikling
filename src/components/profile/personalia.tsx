import React from "react";
import { user } from "../../helpers/business/interfaces";

interface PersonaliaProps {
    userData?: user | null;
}

export const Personalia = ({ userData }: PersonaliaProps) => (
    <>
        {userData && (
            <div className="flex-block mt-4 w-1/4 ">
                <p className="mb-2">
                    <span className=" font-bold">Fornavn:</span>{" "}
                    {userData.firstname}
                </p>
                <p className="my-2">
                    <span className=" font-bold">Etternavn:</span>{" "}
                    {userData.lastname}
                </p>
                <p className="my-2">
                    <span className=" font-bold">Epost:</span> {userData.email}
                </p>
            </div>
        )}
        {!userData && <p className="italic">Ingen bruker funnet</p>}
    </>
);
