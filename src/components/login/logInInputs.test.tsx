import { render } from "@testing-library/react";
import { LogInInputs } from "./logInInputs";
import { BrowserRouter } from "react-router-dom";
import { AuthContextProvider } from "../../context/authContext";
import { act } from "react-dom/test-utils";

describe("LogInInputs", () => {
    test("calls the onClick function when clicked", () => {
        const handleLogIn = jest.fn(); //function mock
        render(
            <div onClick={handleLogIn}>
                <BrowserRouter>
                    <AuthContextProvider>
                        <LogInInputs />
                    </AuthContextProvider>
                </BrowserRouter>
            </div>
        );
        act(() => {
            // fireEvent.click(getByText("Logg inn"));
            expect(handleLogIn).toThrowError();
        });
    });
});
