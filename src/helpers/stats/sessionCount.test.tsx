import { session, emptySession, emptyProgram } from "../business/interfaces";
import { groupByInterval, sessionCountData} from "./sessionCount";

const date1 = new Date(0);
date1.setDate(1);
date1.setMonth(1);
date1.setFullYear(2001);

const date2 = new Date(0);
date2.setDate(9);
date2.setMonth(1);
date2.setHours(2);
date2.setFullYear(2001);

const date3 = new Date(0);
date3.setDate(9);
date3.setMonth(1);
date3.setHours(3);
date3.setFullYear(2001);

const date4 = new Date(0);
date4.setMonth(1);
date4.setFullYear(2000);

const date5 = new Date(0);
date5.setMonth(2);
date5.setFullYear(2000);

const a1 = emptySession();
a1.programName = "lår";
a1.startTime = new Date(date1);

const a2 = emptySession();
a2.programName = "lår";
a2.startTime = new Date(date1);

const b = emptySession();
b.programName = "mage";
b.startTime = new Date(date1);

const c1 = emptySession();
c1.programName = "mage";
c1.startTime = new Date(date2);

const d = emptySession();
d.programName = "mage";
d.startTime = new Date(date3);

const e = emptySession();
e.programName = "mage";
e.startTime = new Date(date4);

const e2 = emptySession();
e2.programName = "mage";
e2.startTime = new Date(date5);

const lårprogram = emptyProgram();
lårprogram.name = "lår"
const mageprogram = emptyProgram();
mageprogram.name = "mage"

const ses:  session[]  = [a1,a2,b,c1, d, e, e2];

it ("Segregates counts correctly into weeks", () => {
    const groups = groupByInterval(ses, "week");
    expect(sessionCountData(groups, lårprogram).map(e => e.y).sort().filter(e => e != 0)).toEqual([2]);
    expect(sessionCountData(groups, mageprogram).map(e => e.y).filter(e => e != 0).sort()).toEqual([1,1,1,2]);
});
it ("Segregates counts correctly into years", () => {
    const groups = groupByInterval(ses, "year");
    expect(sessionCountData(groups, lårprogram).map(e => e.y).sort().filter(e => e != 0)).toEqual([2]);
    expect(sessionCountData(groups, mageprogram).map(e => e.y).filter(e => e != 0).sort()).toEqual([2,3]);
});
it ("Segregates counts correctly into months", () => {
    const groups = groupByInterval(ses, "month");
    expect(sessionCountData(groups, lårprogram).map(e => e.y).sort().filter(e => e != 0)).toEqual([2]);
    expect(sessionCountData(groups, mageprogram).map(e => e.y).filter(e => e != 0).sort()).toEqual([1,1,3]);
});
