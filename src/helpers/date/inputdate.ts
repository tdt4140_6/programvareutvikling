export function UTCInputToDate(dateStr: string): Date {
    return new Date(dateStr + ":00.000Z");
}

export function UTCDateToInput(date: Date): string {
    const dateStr = date.toISOString();
    return dateStr.slice(0, dateStr.lastIndexOf(":"));
}