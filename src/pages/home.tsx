import { Link } from "react-router-dom";
import Header from "../components/header";
import woshareImg from "../img/woshare.jpg";

export const Home = () => (
    <div className="mt-8">
        <Header text={"Hjem"} />
        <img src={woshareImg} alt="hjem" className="mt-8 w-full" />

        <p className="mt-4">
            WO Share er en plattform for å opprette og dele treningsprogram og
            treningsøkter. Du kan også se treningsprogram og treningsøkter som
            andre har laget. Opprett program, legg til øvelser og del med andre.
            Når du har laget et program kan du senere lagre til øktene hvor du
            har utført dette programmet. Dette vil gi deg en ryddig og
            motiverende plattform for å følge opp progresjonen i treningen din.
            Samtidig vil du kunne følge med på venner og andre brukere sine
            treningsøkter og program.
        </p>
        <div className="flex gap-4 w-full my-8">
            <Link to="/createWorkoutProgram" className="w-1/2">
                <button className=" bg-orange-600 text-white py-4 rounded-2xl w-full hover:bg-orange-500">
                    Opprett treningsprogram
                </button>
            </Link>
            <Link to="/createSession" className="w-1/2">
                <button className=" bg-orange-600 text-white py-4 rounded-2xl w-full hover:bg-orange-500">
                    Registrer økt
                </button>
            </Link>
        </div>
    </div>
);
