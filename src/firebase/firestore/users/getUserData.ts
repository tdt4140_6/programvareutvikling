import { getDoc, doc } from "firebase/firestore";
import { user } from "../../../helpers/business/interfaces";
import { firestore } from "../../firebaseConfig";

export const getUser = async (UID: string) => {
    const docRef = doc(firestore, "users", UID);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
        return docSnap.data() as user;
    } else {
        console.log("No such document!");
        return null;
    }
};
