import { getDoc, doc } from "firebase/firestore";
import { user } from "../../../helpers/business/interfaces";
import { firestore } from "../../firebaseConfig";

export const getUserNameByUID = async (UID: string) => {
    const docRef = doc(firestore, "users", UID);
    return await getDoc(docRef).then((doc) => {
        if (doc.exists()) {
            return (doc.data() as user).username;
        } else {
            return null;
        }
    });
};
