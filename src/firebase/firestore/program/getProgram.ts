import { getDoc, doc } from "firebase/firestore";
import { firestore } from "../../firebaseConfig";
import { programDB } from "./getPrograms";

export const getProgram = async (ID: string) => {
    const docRef = doc(firestore, "program", ID);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
        return docSnap.data() as programDB;
    } else {
        console.log("No such document!");
        return null;
    }
};
