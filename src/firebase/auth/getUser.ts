import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useState } from "react";

export function getUser() {
    const [res, setRes] = useState("");
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
        if (user) {
            // User is signed in, see docs for a list of available properties
            // https://firebase.google.com/docs/reference/js/firebase.User
            const uid = user.uid;
            setRes(uid);
        }
    });
    return res;
}
