import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { postUser } from "../firestore/users/userDB";

export function createUser(
    email: string,
    password: string,
    firstname: string,
    lastname: string,
    username: string
) {
    const auth = getAuth();
    return createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            postUser(user.uid, firstname, lastname, email, username, [], []);
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode, errorMessage);
        });
}
