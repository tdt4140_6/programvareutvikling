import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

export function login(email: string, password: string) {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password).catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode, errorMessage);
    });
}
