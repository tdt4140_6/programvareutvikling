# Wo Share

Dette prosjektet ble bygget med npx og [Create React App](https://github.com/facebook/create-react-app).

## Tailwind CSS

Dette prosjektet er bygd med CSS-rammeverket `Tailwind CSS`. Dette er et rammeverk som tillater å stilsette HTML-elementene med inline-tagger.

```
<h1 className="bg-red-500 underline hover:bg-black"></h1>
```

## Tilgjengelige Script

I dette arbeidsområdet kan du kjøre:

### `npm start`

Kjører appen i utviklingsmodus på lokal port.\
Åpne [http://localhost:3000](http://localhost:3000) for å se applikasjonen i nettleseren.

Siden vil automatisk oppdateres på endringer.\
Eventuelle 'linting-errors' vil vises i konsollen.

### `npm test`

Kjører testene i terminalen.\
Les: [kjøring av tester](https://facebook.github.io/create-react-app/docs/running-tests) for mer informasjon.

### `npm run build`

Bygger appen for produksjon til "build-mappen".\
Det bundler React korrekt i produksjonsmodus og optimaliserer bygget for best ytelse.

Filen er optimalisert og filnavnene inkluderer hasher.
Appen er klar for å publiseres!

### `npm run lint`

Kjører linting på kodelageret for å sikre felles formattering uavhengig av hvem som har skrevet koden.

Les [publisering](https://facebook.github.io/create-react-app/docs/deployment) for mer informasjon.

## Se mer

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
